//
//  main.m
//  RJBitTest
//
//  Created by RylanJIN on 03/19/2017.
//  Copyright (c) 2017 RylanJIN. All rights reserved.
//

@import UIKit;
#import "RJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RJAppDelegate class]));
    }
}
