# RJBitTest

[![CI Status](http://img.shields.io/travis/RylanJIN/RJBitTest.svg?style=flat)](https://travis-ci.org/RylanJIN/RJBitTest)
[![Version](https://img.shields.io/cocoapods/v/RJBitTest.svg?style=flat)](http://cocoapods.org/pods/RJBitTest)
[![License](https://img.shields.io/cocoapods/l/RJBitTest.svg?style=flat)](http://cocoapods.org/pods/RJBitTest)
[![Platform](https://img.shields.io/cocoapods/p/RJBitTest.svg?style=flat)](http://cocoapods.org/pods/RJBitTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RJBitTest is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RJBitTest"
```

## Author

RylanJIN, xiaojun.jin@outlook.com

## License

RJBitTest is available under the MIT license. See the LICENSE file for more info.
